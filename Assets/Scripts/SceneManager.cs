using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneManager : MonoBehaviour
{
	[SerializeField] string baseLineScene;
	[SerializeField] List<string> testScenes;
	[SerializeField, Tooltip("%d = sessionDate, %n = testNumber")] string logFileName = "logs/%d/%n.log";
	[SerializeField, Tooltip("%d = sessionDate, %n = testNumber")] string summaryFileName = "logs/%d/%n_summary.log";


	public string sessionDate;
	[System.NonSerialized] public int testNumber = -1;

	public static SceneManager Instance{get; private set;}

	public string LogFileName => logFileName.Replace("%d", sessionDate).Replace("%n", testNumber.ToString());
	public string SummaryLogFileName => summaryFileName.Replace("%d", sessionDate).Replace("%n", testNumber.ToString());

	private void Awake() {
		sessionDate = System.DateTime.Now.ToString("ddMMyyyy_hhmmss");
		if(Instance != null && Instance != this){ 
			Destroy(gameObject);
		}
		Instance = this;
		DontDestroyOnLoad(this);
		for(int i = 0; i< testScenes.Count; i++){
			int second = Random.Range(0, testScenes.Count);
			string temp = testScenes[i];
			testScenes[i] = testScenes[second];
			testScenes[second] = temp;
		}
	}

	private void Start() {
		EventBasedLogger.onTestFinished += RunTest;
		//RunTest();
	}

	private bool baseLine = false;
	int nextScene = 0;
	private void RunTest(){ 
		Application.Quit();
		testNumber++;
		baseLine = !baseLine;
		UnityEngine.SceneManagement.SceneManager.LoadScene(
			baseLine
			? 
			baseLineScene
			:
			testScenes[nextScene++]
		);
		if(nextScene >= testScenes.Count){ 
			Application.Quit();
		}
	}
}
