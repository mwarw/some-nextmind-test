using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NextMind.Calibration;
using NextMind;


public class Calibration : MonoBehaviour{

    [SerializeField]
    private CalibrationManager calibrationManager;

    void Start()
    {
        StartCoroutine(StartCalibrationWhenReady());
    }

    private IEnumerator StartCalibrationWhenReady()
    {
        // Waiting for the NeuroManager to be ready
        yield return new WaitUntil(NeuroManager.Instance.IsReady);

        // Actually start the calibration process.
        calibrationManager.StartCalibration();
    }
}

