using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class EventBasedLogger : MonoBehaviour
{
	public static event System.Action onTestFinished;
	public static EventBasedLogger Instance { get; private set;}
	public static string Date { get; private set;}
	[SerializeField] private string testName; 
	[SerializeField] private Transform player;

	private FileStream logFile;
	private string fileName;
	private List<float> timesTotal = new();
	private List<float> timesWrong = new();
	private List<float> timesWell = new();
	private List<float> distances = new();

	bool innited = false;
	float wrongTimer = 0f;
	float correctTimer = 0f;
	private Vector3 currentPoint;
	private float currentSpotTimestamp;

	private float totalDistanceCurrent = 0f;
	private Vector3 lastLogPosition;

	private void Awake(){
		Instance = this;	
		
	}

	private void Start() {
		fileName = SceneManager.Instance.LogFileName;
		logFile = CreateFile(fileName);
		Log($"startedTestOfType: {testName}");
	}

	public void SetNewSpot(Vector3 point){
		LogPositionOfDrone();
		if(innited){		
			float totalTime = currentSpotTimestamp - Time.time;
			timesTotal.Add(totalTime);			
			Log($"totalTime: {Formatted(totalTime)}");

			timesWrong.Add(wrongTimer);
			Log($"WrongTime: {Formatted(wrongTimer)}");

			timesWell.Add(correctTimer);
			Log($"CorrectTime: {Formatted(correctTimer)}");

			distances.Add(totalDistanceCurrent);
			Log($"DistanceMade: {Formatted(totalDistanceCurrent)}");
		}
		
		wrongTimer = 0f;
		correctTimer = 0f;
		totalDistanceCurrent = 0f;
		innited = true;

		currentSpotTimestamp = Time.time;
		currentPoint = point;		
		LogVector("NewSpot", point);
	}

	private void LogPositionOfDrone(){
		Vector3 newPosition = player.position;
		totalDistanceCurrent += (newPosition - lastLogPosition).magnitude;
		lastLogPosition = newPosition;
		
		LogVector("DronePosition", player.position);

	}
	

	private Vector3 currentDirection;
	public void SetNewVelocity(Vector3 velocity){
		LogPositionOfDrone();
		LogVector("NewVelocity", velocity);
		currentDirection = velocity.normalized;
	}

	public void DumpSummary(){		
		SetNewSpot(Vector3.zero);
		Log("TestEnd");
		FileStream summaryFile = CreateFile(SceneManager.Instance.SummaryLogFileName);
		WriteToStream(
			summaryFile
		,
			$"testType = {testName}\n"+
			$"timeTotal = {Formatted(Sum(timesTotal))}\n" +
			$"wrongTimes = {Formatted(Sum(timesWrong))}\n" +
			$"correctTimes = {Formatted(Sum(timesWell))}\n" +
			$"distance = {Formatted(Sum(distances))}"		
		);
		summaryFile.Close();
		logFile.Close();
		onTestFinished.Invoke();
	}

	private float Sum(List<float> list){
		float sum = 0f;
		foreach(float f in list){
			sum += f;
		}
		return sum;
	}

	private void Update() {
		logFile.Flush();
		if(currentDirection.sqrMagnitude < 0.001f){
			return;
		}

		Vector3 droneToSpot = (currentPoint - player.position).normalized;
		Vector2Int binarizedDroneToSpot = EasyToCompare(droneToSpot);
		Vector2Int binarizedDirection = EasyToCompare(currentDirection);
		Vector2Int sum = binarizedDirection + binarizedDroneToSpot;
		if(sum.x == 2 || sum.x == -2 || sum.y == 2 || sum.y == -2){ 
			correctTimer += Time.deltaTime;
		}else{
			wrongTimer += Time.deltaTime;
		}
	}

	private Vector2Int EasyToCompare(Vector3 v3) => new Vector2Int(GetSign(v3.x), GetSign(v3.y));


	private int GetSign(float f){
		if( f > 0.001f)
			return 1;

		if(f < 0.001f)
			return -1;

		return 0;
	}

	private FileStream CreateFile(string path){
		Debug.Log("Creating file at: "+path);
		FileInfo fileInfo = new FileInfo(path);
		Directory.CreateDirectory(fileInfo.DirectoryName);
		return File.Create(path);
	}

	private void Log(string message){  
		WriteToStream(logFile, $"[{Formatted(Time.time)}] " + message +"\n");
		Debug.Log("logged to logfile:"+ message);
	}

	private void LogVector(string name, Vector3 vector){
		Log($"{name}: ({Formatted(vector.x)},{Formatted(vector.y)},{Formatted(vector.z)})");
	}

	private string Formatted(float f) => f.ToString().Replace(',','.');

	private void WriteToStream(FileStream stream, string text){
		stream.Write(new System.Text.UTF8Encoding(true).GetBytes(text));
	}

}
