using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TelekineticObject : MonoBehaviour{
	[SerializeField] private Vector3 velocity;
	[SerializeField] private Transform objectToMove;
 
	private bool isFocused;

	public void StartFocus() {
		isFocused = true;
		EventBasedLogger.Instance.SetNewVelocity(velocity);
	}

	public void StopFocus(){
		isFocused = false;
		EventBasedLogger.Instance.SetNewVelocity(Vector3.zero);
	}

	private void Update() {
		if(isFocused){
			objectToMove.Translate(velocity*Time.deltaTime);
		}
	}

}
