using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpotSpawned : LoggableEvent{
	readonly Vector2 position;
	public override string EventType => "DotSpawned";
	public override string EventBody => $"";

}
