using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spot : MonoBehaviour{
	//public event System.Action onPointReached;
	
	[SerializeField] private Transform player;
	[SerializeField] private float acceptanceRange = 0.1f;
	[SerializeField, HideInInspector] private float acceptanceRangeSqr = 0.1f*0.1f;

	public bool called = false;

	private void OnValidate() {
		acceptanceRangeSqr = acceptanceRange*acceptanceRange;
	}

	public void ResetFlag() => called = false;

	private void Update() {
		if(!called && (transform.position - player.position).sqrMagnitude < acceptanceRangeSqr){
			called = true;
			//onPointReached.Invoke();
		}
	}

	private void OnDrawGizmosSelected() {
		Gizmos.color = Color.blue;
		Gizmos.DrawWireSphere(transform.position, acceptanceRange);
	}


}
