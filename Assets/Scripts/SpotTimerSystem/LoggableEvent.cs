using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class LoggableEvent{

    public abstract string EventType{get;}
	public abstract string EventBody{get;}

}
