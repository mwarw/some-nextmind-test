using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class SpotManager : MonoBehaviour{
	
	[SerializeField] private List<Vector3> spots;
	[SerializeField] private float spotsStartRange;
	[SerializeField] private Rect worldBounds;
	[SerializeField] private Spot spot;
	[SerializeField] private TMPro.TMP_Text counter;
	[SerializeField] private int startingCounterSeconds;
	private EventBasedLogger Logger => EventBasedLogger.Instance;

	private List<float> timeTable = new List<float>();

	private void Start() {
		StartCoroutine(MainLoop());
	}

	private IEnumerator MainLoop(){
		yield return StartingCount();

		for(int i = 0; i < spots.Count; i++){
			yield return RunSpot(i);
		}

		SaveData();	
	}

	private IEnumerator StartingCount(){ 
		for(int i = startingCounterSeconds; i >= 0; i--){
			counter.text = i.ToString();
			yield return new WaitForSeconds(1f);
		}
		counter.gameObject.SetActive(false);
	}

	private IEnumerator RunSpot(int number){ 
		float startTime = Time.time;
		spot.transform.position = spots[number];
		Logger.SetNewSpot(spot.transform.position);
		yield return new WaitWhile(()=>!spot.called);		
		spot.ResetFlag();
		timeTable.Add(Time.time - startTime);	
	}

	private void SaveData(){
		Logger.DumpSummary();
		Debug.Log("EndedTest");
	}

	private void OnDrawGizmos() {
		Gizmos.color = Color.red;
		Gizmos.DrawLine(new Vector2(worldBounds.xMin, worldBounds.yMin), new Vector2(worldBounds.xMax, worldBounds.yMax));

		Gizmos.color = Color.blue;
		Gizmos.DrawWireSphere(transform.position, spotsStartRange);
	}

	private void OnDrawGizmosSelected() {
		Gizmos.color = Color.green;
				
		Vector3 lastpos = spots[0];
		Gizmos.DrawSphere(lastpos, 0.01f);
		for(int i = 1; i<spots.Count; i++){
			Vector3 newPos = spots[i];
			Gizmos.DrawLine(lastpos, newPos);
			lastpos = newPos;
		}
		
	}


}
