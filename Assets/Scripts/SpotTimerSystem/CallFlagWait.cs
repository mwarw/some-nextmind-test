using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CallFlagWait{
	private bool isLocked = true;

	public void Unlock() => isLocked = false;

	public IEnumerator WaitForUnlock() => new WaitUntil(()=>!isLocked);
}
