using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugLogComponent : MonoBehaviour
{
    [SerializeField] private string message;

	public void Say(){
		Debug.Log(message);
	}

}
